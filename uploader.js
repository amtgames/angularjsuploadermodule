'use strict';

angular.module('amt.uploader', []).
    factory('uploader',function () {
        var uploader = {
            _default_init_options: {
                runtimes: 'html5',
                browse_button: 'pickfiles',
                container: "tmp",
                url: '/this/url/not/work',
                filters: {
                    max_file_size: '1mb',
                    mime_types: [
                        {title: "Plist (amt, binary, xml) Files", extensions: "plist"},
                        {title: "JSON files", extensions: "json"}
                    ]
                }
            },
            _default_init_callbacks: {
                PostInit: function () {
                    $('#pickfiles').removeAttr('disabled');
                    $('#uploadfiles').click(function (e) {
                        $scope.uploader.start();
                        e.preventDefault();
                    });
                },
                FilesAdded: function (up, files) {
                    $('#uploadfiles').removeAttr('disabled');
                    $.each(files, function (i, file) {
                        $('#filelist').append(
                            '<div id="' + file.id + '">' +
                                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                                '</div>');
                    });
                    up.refresh();
                },
                UploadProgress: function (up, file) {
                    $('#' + file.id + " b").html(file.percent + "%");
                    if (file.percent == 100) {
                        var ajax_loader = document.createElement("img");
                        $(ajax_loader).attr('src', "/_admin/static/img/spinner.gif");
                        $('#' + file.id).append("<span style='margin-left: 10px'></span>");
                        $('#' + file.id + " span").html(ajax_loader)
                    }
                },
                Error: function (up, err) {
                    $('#' + err.file.id + " span").html("<span style='color:red; margin-left: 10px'> Error: "
                        + JSON.parse(err.response).error + " </span>");
                    up.refresh();
                },
                FileUploaded: function (up, file) {
                    $('#' + file.id + " b").html("100%");
                    $('#' + file.id + " span").html("<span style='color: green;'>Ok</span>");
                }
            },
            init: function (init_options, init_callbacks) {
                _.extend(this._default_init_options, init_options);
                _.extend(this._default_init_callbacks, init_callbacks);
                this._uploader = new plupload.Uploader(this._default_init_options);
                this._uploader.init();
                var event
                for (event in this._default_init_callbacks) {
                    this._uploader.bind(event, this._default_init_callbacks[event]);
                }
                return this._uploader
            }
        };
        return uploader
    }).
    filter('format_size', function () {
        return function (size) {
            return plupload.formatSize(size)
        }
    });